from selenium.webdriver.common.by import By


class UsernamePageLocators(object):
    username_input = (By.ID, 'identifierId')
    username_confirm_button = (By.ID, 'identifierNext')


class PasswordPageLocators(object):
    password_input = (By.NAME, 'password')
    password_confirm_button = (By.ID, 'passwordNext')


class GmailInboxPageLocators(object):
    compose_button = (By.CLASS_NAME, 'aic')


class MailEditPageLocators(object):
    to_input = (By.NAME, 'to')
    send_button = (By.XPATH, '//*[@class="T-I J-J5-Ji aoO T-I-atl L3"]')
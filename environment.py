import unittest, page, time
from selenium import webdriver

class SendEmptyEmail(unittest.TestCase):

    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--incognito')

        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.get("https://www.google.com/gmail/")

    def test_send_empty_email(self):
        username = 'email@gmail.com'
        password = 'password'
        to = username

        #enter username
        current_page = page.UsernameInputPage(self.driver)
        current_page.enter_username(username)
        current_page.click_confirm()
        time.sleep(2)

        #enter password
        current_page = page.PasswordInputPage(self.driver)
        current_page.enter_password(password)
        current_page.click_confirm()
        time.sleep(4)

        #compose email
        current_page = page.GmailInboxPage(self.driver)
        current_page.click_compose()
        time.sleep(1)

        #send email
        current_page = page.MailEditPage(self.driver)
        current_page.enter_to(to)
        current_page.click_send()
        time.sleep(1)
        self.driver.switch_to.alert.accept()
        time.sleep(3)


    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
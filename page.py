from element import BasePageElement
from locators import *


class BasePage(object):
    def __init__(self, driver):
        self.driver = driver


class UsernameInputPage(BasePage):

    def enter_username(self, username):
        element = self.driver.find_element(*UsernamePageLocators.username_input)
        element.send_keys(username)

    def click_confirm(self):
        element = self.driver.find_element(*UsernamePageLocators.username_confirm_button)
        element.click()


class PasswordInputPage(BasePage):

    def enter_password(self, password):
        element = self.driver.find_element(*PasswordPageLocators.password_input)
        element.send_keys(password)

    def click_confirm(self):
        element = self.driver.find_element(*PasswordPageLocators.password_confirm_button)
        element.click()


class GmailInboxPage(BasePage):

    def click_compose(self):
        element = self.driver.find_element(*GmailInboxPageLocators.compose_button)
        element.click()


class MailEditPage(BasePage):

    def enter_to(self, to):
        element = self.driver.find_element(*MailEditPageLocators.to_input)
        element.send_keys(to)

    def click_send(self):
        element = self.driver.find_element(*MailEditPageLocators.send_button)
        element.click()
